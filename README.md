
# Especificações de Requisitos de Gilded Rose

Bem-vindo ao time Gilded Rose. Como você deve saber, nós somos uma pequena pousada estrategicamente localizada em uma prestigiosa cidade, atendida pelo amigável atendente Allison. Além de ser uma pousada, nós também compramos e vendemos as mercadorias de melhor qualidade. Infelizmente nossas mercadorias vão perdendo a qualidade conforme chegam próximo sua data de venda.

Nós temos um sistema instalado que atualiza automaticamente os preços do nosso estoque. Esse sistema foi criado por um rapaz sem noção chamado Leeroy, que agora se dedica à novas aventuras. Seu trabalho será adicionar uma nova funcionalidade para o nosso sistema para que possamos vender uma nova categoria de itens. 

## Descrição preliminar

Vamos dar uma breve introdução do nosso sistema:

* Todos os itens (classe `Item`) possuem uma propriedade chamada `PrazoParaVenda` que informa o número de dias que temos para vende-lo
* Todos os itens possuem uma propriedade chamada `Qualidade` que informa o quão valioso é o item.
* No final do dia, nosso sistema decrementa os valores das propriedades `PrazoParaVenda` e `Qualidade` de cada um dos itens do estoque através do método `AtualizarQualidade`.

Bastante simples, não é? Bem, agora que as coisas ficam interessantes:

* Quando o (`PrazoParaVenda`) do item tiver passado, a (`Qualidade`) do item diminui duas vezes mais rápido.
* A (`Qualidade`) do item não pode ser negativa
* O (`Queijo Brie envelhecido`), aumenta sua qualidade (`Qualidade`) em `1` unidade a medida que envelhece.
* A (`Qualidade`) de um item não pode ser maior que 50.
* O item (`Dente do Tarrasque`), por ser um item lendário, não precisa ter um (`PrazoParaVenda`) e sua (`Qualidade`) não precisa ser diminuída.
* O item (`Ingressos`), assim como o (`Queijo Brie envelhecido`), aumenta sua (`Qualidade`) a medida que o  (`PrazoParaVenda`) se aproxima;
    * A (`Qualidade`) aumenta em `2` unidades quando o (`PrazoParaVenda`) é igual ou menor que `10`.
    * A (`Qualidade`) aumenta em `3` unidades quando o (`PrazoParaVenda`) é igual ou menor que `5`.
    * A (`Qualidade`) do item vai direto à `0` quando o (`PrazoParaVenda`) tiver passado.

Nós recentemente assinamos um suprimento de itens Conjurados Magicamente. Isto requer que nós atualizemos nosso sistema:

* Os itens "Conjurados" (`Conjurado`) diminuem a (`Qualidade`) duas vezes mais rápido que os outros itens.

Sinta-se livre para fazer qualquer alteração no método `AtualizarQualidade` e adicionar código novo contanto que tudo continue funcionando perfeitamente. Entretanto, não altere o código da classe `Item` ou da propriedade `Items` na classe `GildedRose` pois elas pertencem a um Goblin que irá te matar com um golpe pois ele não acredita na cultura de código compartilhado.

## Notas Finais

Para esclarecer: Um item não pode ter uma (`Qualidade`) maior que `50`, entretanto o  (`Dente do Tarrasque`) por ser um item lendário vai ter uma qualidade imutável de `80`.

# Solução Implementada

Identifiquei alguns pontos de melhoria no código original e fiz alguns ajustes no escopo da refatoração do código. Abaixo descrevo o que foi feito.

## Aplicação do design pattern The Rules
Foi aplicado o Rules Design Pattern com o objetivo de garantir a aplicação dos princípios de responsabilidade única e aberto/fechado.
Existem várias regras para atualização da qualidade com base no tipo de item que está sendo tratado e também do prazo de venda em que este item se encontra. Estas regras foram separadas
em classes especialistas. Estas classes especialistas tratam cada regra individualmente e todas elas implementam a interface `IRegraDeNegocio`. A classe `GildedRose` ficou com a responsabilidade
de mapear as regras de negócio para seus respectivos conjuntos de items e executar tais regras. Ex.:

```
public GildedRose(IList<Item> Itens)
{
    this.Itens = Itens;
    _itensConjurados = Itens
        .Where(item => item.Nome.ToLower().Contains("conjurado"))
        .ToList();
    _itensEspeciais = Itens
        .Where(item => 
            "queijo brie envelhecido" == item.Nome.ToLower()
            || "ingressos para o concerto do turisas" == item.Nome.ToLower())
        .ToList();
    _itensLendarios = Itens
        .Where(item => "dente do tarrasque" == item.Nome.ToLower())
        .ToList();
    _itensComuns = Itens.Where(item => !_itensConjurados.Any(conjurado => conjurado.Nome == item.Nome)
        && !_itensEspeciais.Any(especial => especial.Nome == item.Nome)
        && !_itensLendarios.Any(lendario => lendario.Nome == item.Nome)).ToList();
    MapearRegrasItensComuns();
    MapearRegrasItensConjurados();
    MapearRegrasIngressos();
    MapearRegrasQueijos();
    MapearRegrasPrazoDeVenda();
}
public void ExecutarRegras()
{
    foreach (KeyValuePair<IRegraDeNegocio, IList<Item>> regra in _regrasParaAtualizarQualidade)
    {
        regra.Key.Executar(regra.Value);
    }
}

private void MapearRegrasItensConjurados() =>
    // Os itens conjurados diminuem a 
    // qualidade duas vezes mais rápido
    _regrasParaAtualizarQualidade.Add(
        new RegraItemConjurado(
            _decrementador, _decrementoPadrao * 2), _itensConjurados);
``` 

## Refatoração do nome do método público de GildedRose
O método `AtualizarQualidade()`, na versão original do código, estava com mais de uma responsabilidade, pois executava a atualização de quantidade mas também atualizava o prazo de venda. Este método foi renomeado para `ExecutarRegras()` e agora passa a realizar a chamada das classes especialistas.

## Por que optei pela aplicação deste pattern?
O principal agente motivador da escolha deste pattern foi a possibilidade de alterar uma regra existente sem interferir nas demais, e também a possibilidade de adicionar regras novas de forma mais fácil, tendo o trabalho apenas de implementar esta regra em uma classe especialista, mapeando-a ao seu sub conjunto de itens relacionado e inserindo-a para execução na classe `GildedRose`.

## O problema do Goblin
O Goblin assasino impediu que o design desta solução ficasse um pouco mais amigável. Como tive que lidar com a preciosidade deste Goblin, não pude alterar a classe `Item` e adicionar alguns atalhos que teriam economizado algumas classes. Provavelmente iria concentrar regras inerentes ao domínio nesta classe, fazendo com que a aplicação do padrão The Rules fosse mais "bonita".

## Testes automatizados

Alguns testes unitários foram implementados. No entanto, não implementei todos, tomaria algum tempo a mais. Optei por implementar alguns e espero ter conseguido demonstrar a capacidade e proficiência no framework.

Uma vez que o design da solução impactou diretamente na classe `GildedRose` o teste desta classe foi ignorado. O mapeamento das regras bem como a chamada às classes especialistas não foram incluídas nos testes automatizados. Isto demandaria o desenvolvimento de um mecanismo de injeção de dependências decente para injetar as implementações especialistas, tirando a respondabilidade de instanciar cada uma dentro desta classe.
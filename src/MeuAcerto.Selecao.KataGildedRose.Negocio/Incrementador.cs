namespace MeuAcerto.Selecao.KataGildedRose.Negocio
{
    public class Incrementador : IIncrementador
    {
        public int Incrementar(int valor, int incremento)
        {
            valor += incremento;

            return valor < 50 ? valor : 50;
        }
    }
}

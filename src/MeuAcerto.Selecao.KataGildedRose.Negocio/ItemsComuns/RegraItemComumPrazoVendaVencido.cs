using System.Collections.Generic;
using System.Linq;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsComuns
{
    public class RegraItemComumPrazoVendaVencido : IRegraDeNegocio
    {
        readonly int _decremento;
        readonly int _prazoVendaVencido;
        readonly IDecrementador _decrementador;

        public RegraItemComumPrazoVendaVencido(
            IDecrementador decrementador,
            int decremento,
            int prazoVendaVencido)
        {
            _decrementador = decrementador;
            _prazoVendaVencido = prazoVendaVencido;
            _decremento = decremento;
        }

        public void Executar(IList<Item> itens)
        {
            itens
                .Where(item => item.PrazoParaVenda <= _prazoVendaVencido)
                .ToList()
                .ForEach(item => {
                    item.Qualidade = _decrementador
                        .Decrementar(item.Qualidade, _decremento);
                });
        }
    }
}
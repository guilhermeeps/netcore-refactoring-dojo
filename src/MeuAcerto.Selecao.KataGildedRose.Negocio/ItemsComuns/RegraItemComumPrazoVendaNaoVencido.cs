using System.Collections.Generic;
using System.Linq;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsComuns
{
    public class RegraItemComumPrazoVendaNaoVencido : IRegraDeNegocio
    {
        readonly int _decremento;
        readonly int _prazoVenda;
        readonly IDecrementador _decrementador;

        public RegraItemComumPrazoVendaNaoVencido(
            IDecrementador decrementador,
            int decremento,
            int prazoVenda)
        {
            _decrementador = decrementador;
            _prazoVenda = prazoVenda;
            _decremento = decremento;
        }

        public void Executar(IList<Item> itens)
        {
            itens
                .Where(item => item.PrazoParaVenda > _prazoVenda)
                .ToList()
                .ForEach(item => {
                    item.Qualidade = _decrementador
                        .Decrementar(item.Qualidade, _decremento);
                });
        }
    }
}
using System.Collections.Generic;
using System.Linq;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsConjurados
{
    public class RegraItemConjurado : IRegraDeNegocio
    {
        readonly int _qualidade;
        readonly IDecrementador _decrementador;

        public RegraItemConjurado(
            IDecrementador decrementador,
            int qualidade)
        {
            _decrementador = decrementador;
            _qualidade = qualidade;
        }

        public void Executar(IList<Item> itens)
        {
            itens
                .ToList()
                .ForEach(item => {
                    item.Qualidade = _decrementador
                        .Decrementar(item.Qualidade, _qualidade);
                });
        }
    }
}
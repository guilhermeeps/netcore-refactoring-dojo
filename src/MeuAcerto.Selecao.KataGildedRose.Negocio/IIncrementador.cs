namespace MeuAcerto.Selecao.KataGildedRose.Negocio
{
    public interface IIncrementador
    {
        int Incrementar(int valor, int incremento);
    }
}

﻿using System.Linq;
using System.Collections.Generic;

using MeuAcerto.Selecao.KataGildedRose.Entidade;
using MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsComuns;
using MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsConjurados;
using MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsEspeciais;
using MeuAcerto.Selecao.KataGildedRose.Negocio.PrazoVenda;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio
{
    public class GildedRose
    {
        readonly int _decrementoPadrao = 1;
        readonly int _incrementoPadrao = 1;
        readonly IList<Item> _itensLendarios;
        readonly IList<Item> _itensEspeciais;
        readonly IList<Item> _itensConjurados;
        readonly IList<Item> _itensComuns;
        readonly IDecrementador _decrementador = new Decrementador();
        readonly IIncrementador _incrementador = new Incrementador();
        readonly IDictionary<IRegraDeNegocio, IList<Item>> _regrasParaAtualizarQualidade = 
            new Dictionary<IRegraDeNegocio, IList<Item>>();
        readonly IList<Item> Itens;

        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
            _itensConjurados = Itens
                .Where(item => item.Nome.ToLower().Contains("conjurado"))
                .ToList();
            _itensEspeciais = Itens
                .Where(item => 
                    "queijo brie envelhecido" == item.Nome.ToLower()
                    || "ingressos para o concerto do turisas" == item.Nome.ToLower())
                .ToList();
            _itensLendarios = Itens
                .Where(item => "dente do tarrasque" == item.Nome.ToLower())
                .ToList();
            _itensComuns = Itens.Where(item => !_itensConjurados.Any(conjurado => conjurado.Nome == item.Nome)
                && !_itensEspeciais.Any(especial => especial.Nome == item.Nome)
                && !_itensLendarios.Any(lendario => lendario.Nome == item.Nome)).ToList();

            MapearRegrasItensComuns();
            MapearRegrasItensConjurados();
            MapearRegrasIngressos();
            MapearRegrasQueijos();
            MapearRegrasPrazoDeVenda();
        }

        public void ExecutarRegras()
        {
            foreach (KeyValuePair<IRegraDeNegocio, IList<Item>> regra in _regrasParaAtualizarQualidade)
            {
                regra.Key.Executar(regra.Value);
            }
        }

        private void MapearRegrasQueijos() 
        {
            var queijos = _itensEspeciais
                .Where(item => "queijo brie envelhecido" == item.Nome.ToLower())
                .ToList();
            // O (Queijo Brie envelhecido), aumenta sua qualidade (Qualidade) em 1 unidade
            // a medida que envelhece.
            _regrasParaAtualizarQualidade.Add(
                new RegraQueijo(_incrementador, _incrementoPadrao), queijos);
        }

        private void MapearRegrasIngressos()
        {
            var ingressos = _itensEspeciais
                .Where(item => "ingressos para o concerto do turisas" == item.Nome.ToLower())
                .ToList();
            // Itens especiais
            // A (Qualidade) do item vai direto à 0 quando o 
            // (PrazoParaVenda) tiver passado.
            _regrasParaAtualizarQualidade.Add(
                new RegraIngressosPrazoVendaVencido(0, 0), ingressos);
            
            // A (Qualidade) aumenta em 3 unidades quando o 
            // (PrazoParaVenda) é igual ou menor que 5.
            _regrasParaAtualizarQualidade.Add(
                new RegraIngressosPrazoVendaCurto(
                    _incrementador, 5, 3), ingressos);
            
            // A (Qualidade) aumenta em 2 unidades quando o 
            // (PrazoParaVenda) é igual ou menor que 10.
            _regrasParaAtualizarQualidade.Add(
                new RegraIngressosPrazoVendaMedio(
                    _incrementador, 5, 10, 2), ingressos);
            
            // Demais casos para itens especiais qualidade
            // incremeta normalmente
            _regrasParaAtualizarQualidade.Add(
                new RegraIngressosPrazoVendaLongo(
                    _incrementador,  11, 1), ingressos);
        }

        private void MapearRegrasItensConjurados() =>
            // Os itens conjurados diminuem a 
            // qualidade duas vezes mais rápido
            _regrasParaAtualizarQualidade.Add(
                new RegraItemConjurado(
                    _decrementador, _decrementoPadrao * 2), _itensConjurados);

        private void MapearRegrasItensComuns() {
            // Quando o prazo de validade de itens
            // não lendários e não especiais 
            // não tiver passado decrementa normalmente
            _regrasParaAtualizarQualidade.Add(
                new RegraItemComumPrazoVendaNaoVencido(
                    _decrementador, _decrementoPadrao, 0), _itensComuns);
            
            // Quando o prazo de validade de itens
            // não lendários e não especiais 
            // não tiver passado decrementa 
            // duas vezes mais rápido
            _regrasParaAtualizarQualidade.Add(
                new RegraItemComumPrazoVendaVencido(
                    _decrementador, _decrementoPadrao * 2, 0), _itensComuns);
        }

        private void MapearRegrasPrazoDeVenda() => 
            // itens lendários não têm prazo de venda
            _regrasParaAtualizarQualidade.Add(
                new RegraPrazoDeVenda(_decrementoPadrao), 
                    this.Itens
                        .Where(item => !_itensLendarios
                        .Any(lendario => lendario.Nome == item.Nome))
                        .ToList());
    }
}

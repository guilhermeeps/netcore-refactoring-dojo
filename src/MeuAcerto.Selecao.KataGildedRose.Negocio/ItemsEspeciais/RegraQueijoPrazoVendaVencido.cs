using System.Collections.Generic;
using System.Linq;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsEspeciais
{
    public class RegraQueijoPrazoVendaVencido : IRegraDeNegocio
    {
        readonly int _incremento;
        readonly IIncrementador _incrementador;
        readonly int _prazoVendaMinimo;

        public RegraQueijoPrazoVendaVencido(
            IIncrementador incrementador, 
            int prazoVendaMinimo,
            int incremento)
        {
            _incrementador = incrementador;
            _prazoVendaMinimo = prazoVendaMinimo;
            _incremento = incremento;
        }

        public void Executar(IList<Item> itens)
        {
            itens
                .Where(item => item.PrazoParaVenda < _prazoVendaMinimo)
                .ToList()
                .ForEach(item => {
                    item.Qualidade  = _incrementador
                        .Incrementar(item.Qualidade, _incremento);
                });
        }
    }
}
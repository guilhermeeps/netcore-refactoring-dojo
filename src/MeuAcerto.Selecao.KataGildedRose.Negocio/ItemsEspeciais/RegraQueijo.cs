using System.Collections.Generic;
using System.Linq;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsEspeciais
{
    public class RegraQueijo : IRegraDeNegocio
    {
        readonly int _incremento;
        readonly IIncrementador _incrementador;

        public RegraQueijo(
            IIncrementador incrementador, 
            int incremento)
        {
            _incrementador = incrementador;
            _incremento = incremento;
        }

        public void Executar(IList<Item> itens)
        {
            itens
                .ToList()
                .ForEach(item => {
                    item.Qualidade  = _incrementador
                        .Incrementar(item.Qualidade,  item.PrazoParaVenda <= 0 ? _incremento * 2 : _incremento);
                });
        }
    }
}
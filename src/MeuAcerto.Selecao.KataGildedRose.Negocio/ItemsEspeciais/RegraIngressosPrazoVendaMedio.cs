using System.Collections.Generic;
using System.Linq;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsEspeciais
{
    public class RegraIngressosPrazoVendaMedio : IRegraDeNegocio
    {
        readonly int _incremento;
        readonly int _inicioPeriodo;
        readonly int _fimPeriodo;
        readonly IIncrementador _incrementador;

        public RegraIngressosPrazoVendaMedio(
            IIncrementador incrementador,
            int inicioPeriodo,
            int fimPeriodo,
            int incremento)
        {
            _incrementador = incrementador;
            _inicioPeriodo = inicioPeriodo;
            _fimPeriodo = fimPeriodo;
            _incremento = incremento;
        }

        public void Executar(IList<Item> itens)
        {
            itens
                .Where(item => 
                    item.PrazoParaVenda > _inicioPeriodo
                    && item.PrazoParaVenda <= _fimPeriodo)
                .ToList()
                .ForEach(item => {
                    item.Qualidade = _incrementador
                        .Incrementar(item.Qualidade, _incremento);
                });
        }
    }
}
using System.Collections.Generic;
using System.Linq;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsEspeciais
{
    public class RegraIngressosPrazoVendaVencido : IRegraDeNegocio
    {
        readonly int _qualidade;
        readonly int _prazoVenda;

        public RegraIngressosPrazoVendaVencido(
            int qualidade,
            int prazoVenda)
        {
            _qualidade = qualidade;
            _prazoVenda = prazoVenda;
        }

        public void Executar(IList<Item> itens)
        {
            itens
                .Where(item => item.PrazoParaVenda <= _prazoVenda)
                .ToList()
                .ForEach(item => {
                    item.Qualidade = _qualidade;
                });  
        }
    }
}
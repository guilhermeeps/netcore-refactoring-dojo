using System.Collections.Generic;
using System.Linq;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsEspeciais
{
    public class RegraIngressosPrazoVendaLongo : IRegraDeNegocio
    {
        readonly int _incremento;
        readonly int _prazoVenda;
        readonly IIncrementador _incrementador;

        public RegraIngressosPrazoVendaLongo(
            IIncrementador incrementador,
            int prazoVenda,
            int incremento)
        {
            _incrementador = incrementador;
            _prazoVenda = prazoVenda;
            _incremento = incremento;
        }

        public void Executar(IList<Item> itens)
        {
            itens
                .Where(item => item.PrazoParaVenda >= _prazoVenda)
                .ToList()
                .ForEach(item => {
                    item.Qualidade = _incrementador
                        .Incrementar(item.Qualidade, _incremento);
                });
        }
    }
}
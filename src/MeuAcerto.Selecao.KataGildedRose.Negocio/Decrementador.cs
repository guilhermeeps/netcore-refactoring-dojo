namespace MeuAcerto.Selecao.KataGildedRose.Negocio
{
    public class Decrementador : IDecrementador
    {
        public int Decrementar(int valor, int decremento)
        {
            valor -= decremento;

            return valor > 0 ? valor : 0;
        }
    }
}
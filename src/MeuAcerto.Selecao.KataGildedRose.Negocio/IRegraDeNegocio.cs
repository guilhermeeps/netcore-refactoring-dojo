using System.Collections.Generic;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio
{
    public interface IRegraDeNegocio
    {
         void Executar(IList<Item> itens);
    }
}
using System.Linq;
using System.Collections.Generic;
using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Negocio.PrazoVenda
{
    public class RegraPrazoDeVenda : IRegraDeNegocio
    {
        readonly int _decremento;

        public RegraPrazoDeVenda(int decremento) =>
            this._decremento = decremento;

        public void Executar(IList<Item> itens) =>
            itens
                .ToList()
                .ForEach(item => item.PrazoParaVenda -= _decremento);
    }
}
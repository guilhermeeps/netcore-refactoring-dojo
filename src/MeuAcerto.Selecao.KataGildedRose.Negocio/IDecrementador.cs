namespace MeuAcerto.Selecao.KataGildedRose.Negocio
{
    public interface IDecrementador
    {
        int Decrementar(int valor, int decremento);
    }
}
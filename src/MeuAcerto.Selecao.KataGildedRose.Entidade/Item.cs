﻿namespace MeuAcerto.Selecao.KataGildedRose.Entidade
{
    public class Item
    {
        public string Nome { get; set; }

        public int PrazoParaVenda { get; set; }

        public int Qualidade { get; set; }
    }
}

using Xunit;

using MeuAcerto.Selecao.KataGildedRose.Negocio;

namespace MeuAcerto.Selecao.KataGildedRose.Tests.Negocio
{   
    public class IncrementadorTest
    {
        readonly IIncrementador incrementador;

        public IncrementadorTest() => incrementador = new Incrementador();
    
        [Theory]
        [InlineData(1, 1, 2)]
        [InlineData(0, 1, 1)]
        [InlineData(10, 5, 15)]
        [InlineData(40, 11, 50)] // valor máximo de 50
        public void Incrementar_DeveIncrementarUmValor(
            int valor, int incremento, int resultadoEsperado)
        {
            var resultado = incrementador.Incrementar(valor, incremento);

            Assert.Equal(resultadoEsperado, resultado);
        }
        
    }
}
using Xunit;

using MeuAcerto.Selecao.KataGildedRose.Negocio;

namespace MeuAcerto.Selecao.KataGildedRose.Tests.Negocio
{
    public class DecrementadorTest
    {
        readonly IDecrementador decrementador;

        public DecrementadorTest() => decrementador = new Decrementador();
    
        [Theory]
        [InlineData(2, 1, 1)]
        [InlineData(10, 2, 8)]
        [InlineData(0, 2, 0)] // não pode ter valor menor que zero
        public void Incrementar_DeveDecrementarUmValor(
            int valor, int decremento, int resultadoEsperado)
        {
            var resultado = decrementador.Decrementar(valor, decremento);

            Assert.Equal(resultadoEsperado, resultado);
        }
    }
}
using NSubstitute;
using System.Reflection;
using System.Collections.Generic;
using System;
using System.Linq;
using Xunit;

using MeuAcerto.Selecao.KataGildedRose.Entidade;
using MeuAcerto.Selecao.KataGildedRose.Negocio;
using MeuAcerto.Selecao.KataGildedRose.Negocio.PrazoVenda;

namespace MeuAcerto.Selecao.KataGildedRose.Tests.Negocio.PrazoVenda
{
    public class RegraPrazoDeVendaTest
    {
        IRegraDeNegocio regraPrazoDeVenda;

        [Theory]
        [ClassData(typeof(ItemsTestData.TodosOsItemsComPrazoDeVendaIgualADez))]
        public void Executar_DeveExecutarRegraParaPrazoDeVendaNaoVencido(IList<Item> itens) 
        {
            regraPrazoDeVenda = new RegraPrazoDeVenda(1);

            regraPrazoDeVenda.Executar(itens);
            
            var itensComPrazoDeVendaInalterado = itens.Where(i => i.PrazoParaVenda > 9).ToList();
            Assert.Empty(itensComPrazoDeVendaInalterado);
        }
        
    }
}
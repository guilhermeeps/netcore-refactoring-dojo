using NSubstitute;
using System.Reflection;
using System.Collections.Generic;
using System;
using System.Linq;
using Xunit;

using MeuAcerto.Selecao.KataGildedRose.Entidade;
using MeuAcerto.Selecao.KataGildedRose.Negocio;
using MeuAcerto.Selecao.KataGildedRose.Negocio.ItemsComuns;

namespace MeuAcerto.Selecao.KataGildedRose.Tests.Negocio.ItemsComuns
{
    public class RegraItemComumPrazoVendaNaoVencidoTest
    {
        readonly IDecrementador decrementador;
        IRegraDeNegocio regraItemComumPrazoNaoVencido;

        public RegraItemComumPrazoVendaNaoVencidoTest() =>
            decrementador = Substitute.For<IDecrementador>();

        [Theory]
        [ClassData(typeof(ItemsTestData.ItemsComuns))]
        public void Executar_DeveExecutarRegraParaPrazoDeVendaNaoVencido(IList<Item> itens) 
        {
            int decremento = 1; // itens com prazo de venda não vencido
            int prazoVendaLimite = 0; // abaixo deste valor prazo de venda está vencido
            decrementador.Decrementar(Arg.Any<int>(), decremento).Returns(1);
            regraItemComumPrazoNaoVencido = 
                new RegraItemComumPrazoVendaNaoVencido(decrementador, decremento, prazoVendaLimite);

            regraItemComumPrazoNaoVencido.Executar(itens);
            
            var itensAtualizados = itens.Where(i => i.Qualidade == 1).ToList();
            Assert.True(itensAtualizados.Count == 2); // apenas dois itens foram atualizados
        }
    }
}
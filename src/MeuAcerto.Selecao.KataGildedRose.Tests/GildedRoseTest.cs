﻿using System.Collections.Generic;
using Xunit;

using MeuAcerto.Selecao.KataGildedRose.Entidade;
using MeuAcerto.Selecao.KataGildedRose.Negocio;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class GildedRoseTest
    {
        [Fact(Skip="Precisa ser redesenhado.")]
        public void foo()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "foo", PrazoParaVenda = 0, Qualidade = 0 } };
            GildedRose app = new GildedRose(Items);
            app.ExecutarRegras();
            Assert.Equal("fixme", Items[0].Nome);
        }
    }
}

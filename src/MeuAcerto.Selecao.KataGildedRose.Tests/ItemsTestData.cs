using System.Collections.Generic;
using Xunit;

using MeuAcerto.Selecao.KataGildedRose.Entidade;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    internal class ItemsTestData
    {
        internal class ItemsComuns : TheoryData<IList<Item>> 
        {
            public ItemsComuns()
            {
                IList<Item> items = new List<Item>
                {
                    new Item {Nome = "Elixir do Mangusto", PrazoParaVenda = 10, Qualidade = 2},
                    new Item {Nome = "Elixir do Mangusto", PrazoParaVenda = 5, Qualidade = 2},
                    new Item {Nome = "Elixir do Mangusto", PrazoParaVenda = 0, Qualidade = 0},
                    new Item {Nome = "Corselete +5 DEX", PrazoParaVenda = -3, Qualidade = 0}
                };
                Add(items);  
            }
        }

        internal class TodosOsItemsComPrazoDeVendaIgualADez : TheoryData<IList<Item>>
        {
            public TodosOsItemsComPrazoDeVendaIgualADez()
            {
                IList<Item> items = new List<Item>
                {
                    new Item {Nome = "Corselete +5 DEX", PrazoParaVenda = 10, Qualidade = 20},
                    new Item {Nome = "Queijo Brie Envelhecido", PrazoParaVenda = 10, Qualidade = 0},
                    new Item {Nome = "Elixir do Mangusto", PrazoParaVenda = 10, Qualidade = 7},
                    new Item {Nome = "Dente do Tarrasque", PrazoParaVenda = 10, Qualidade = 80},
                    new Item {Nome = "Dente do Tarrasque", PrazoParaVenda = 10, Qualidade = 80},
                    new Item
                    {
                        Nome = "Ingressos para o concerto do Turisas",
                        PrazoParaVenda = 10,
                        Qualidade = 20
                    },
                    new Item
                    {
                        Nome = "Ingressos para o concerto do Turisas",
                        PrazoParaVenda = 10,
                        Qualidade = 49
                    },
                    new Item
                    {
                        Nome = "Ingressos para o concerto do Turisas",
                        PrazoParaVenda = 10,
                        Qualidade = 49
                    },
                    // Este item conjurado ainda não funciona direto!
                    new Item {Nome = "Bolo de Mana Conjurado", PrazoParaVenda = 10, Qualidade = 6}
			    };
                Add(items);
            }
        }
    }
}